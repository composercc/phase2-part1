package server;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author liJin
 * @date 2021/1/10 16:01
 * @desc Minicat的主类
 */
public class Bootstrap {

    /**
     * 定义socket监听的端口号
     */
    private int port = 8080;

    private String appBase;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    private Map<String, HttpServlet> servletMap = new HashMap<>();

    private void loadServerXML() {
        InputStream inputStream = Bootstrap.class.getClassLoader().getResourceAsStream("server.xml");
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(inputStream);
            Element rootElement = document.getRootElement();
            Node portNode = rootElement.selectSingleNode("/Server/services/Connector/@port");
            this.port = Integer.parseInt(portNode.getStringValue());
            System.out.println("配置 port = " + this.port);
            Node appBaseNode = rootElement.selectSingleNode("/Server/services/Engine/Host/@appBase");
            this.appBase = appBaseNode.getStringValue();
            System.out.println("配置 appBase = " + this.appBase);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 加载解析web.xml，初始化Servlet
     */
    private void loadServlet(File projectFile) {
        FileInputStream resourceAsStream;
        SAXReader saxReader = new SAXReader();
        String path = projectFile.getAbsolutePath();
        MyClassLoader myClassLoader = new MyClassLoader(path);
        //myClassLoader.setClasspath(path);
        File webxml = new File(path + File.separator + "web.xml");
        if (webxml.exists()) {
            try {
                resourceAsStream = new FileInputStream(webxml);
                Document document = saxReader.read(resourceAsStream);
                Element rootElement = document.getRootElement();

                List<Element> selectNodes = rootElement.selectNodes("//servlet");
                for (int i = 0; i < selectNodes.size(); i++) {
                    Element element = selectNodes.get(i);
                    //  <servlet-name>myServlet</servlet-name>
                    Element servletNameElement = (Element) element.selectSingleNode("servlet-name");
                    String servletName = servletNameElement.getStringValue();
                    //  <servlet-class>server.MyServlet</servlet-class>
                    Element servletClassElement = (Element) element.selectSingleNode("servlet-class");
                    String servletClass = servletClassElement.getStringValue();

                    // 根据servlet-name的值找到url-pattern
                    Element servletMappingElement = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                    Element urlPatternElement = (Element) servletMappingElement.selectSingleNode("url-pattern");
                    // /lagou
                    String urlPattern = urlPatternElement.getStringValue();
                    //将路径加入到类加载器
                    Class clazz = myClassLoader.loadClass(servletClass);
                    servletMap.put(urlPattern, (HttpServlet) clazz.newInstance());
                }
            } catch (DocumentException | ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Minicat启动需要初始化展开的一些操作
     */
    public void start() throws Exception {
        // 解析server.xml
        loadServerXML();
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("===========>>>>>> Minicat start on port: " + port);
        // 加载Servlet
        File webappsFile = new File(this.appBase);
        if (webappsFile.exists()) {
            //判断是否是目录，然后进行遍历
            if (webappsFile.isDirectory()) {
                File[] projectFiles = webappsFile.listFiles();
                for (File projectFile : projectFiles) {
                    //解析相应的servlet文件夹下面的web.xml
                    if (projectFile.isDirectory()) {
                        loadServlet(projectFile);
                    }
                }
            }
        } else {
            System.out.println("webapps文件夹不存在!");
        }
        int corePoolSize = 10;
        int maximumPoolSize = 50;
        long keepAliveTime = 100L;
        TimeUnit unit = TimeUnit.SECONDS;
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(50);
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();
        // 线程池
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
        // Minicat 1.0
        // 需求：浏览器请求http://localhost:8080,返回一个固定的字符串到页面"hello Minicat!"
//        while (true) {
//            Socket socket = serverSocket.accept();
//            // 有了socket,接收到请求，获取输出流
//            OutputStream outputStream = socket.getOutputStream();
//            String data = " Hello Minicat!";
//            String responseText = HttpProtocolUtil.getHttpHeader200(data.getBytes().length) + data;
//            outputStream.write(responseText.getBytes());
//            socket.close();
//        }
        // Minicat 2.0
        // 需求：封装Request和Response对象，返回html对象
//        while (true) {
//            Socket socket = serverSocket.accept();
//            InputStream inputStream = socket.getInputStream();
//            OutputStream outputStream = socket.getOutputStream();
//            // 封装Request对象和Response对象
//            Request request = new Request(inputStream);
//            Response reSponse = new Response(outputStream);
//            reSponse.outputHtml(request.getUrl());
//            socket.close();
//        }
        // Minicat 3.0
        // 需求：请求动态资源（Servlet）
//        while (true) {
//            Socket socket = serverSocket.accept();
//            InputStream inputStream = socket.getInputStream();
//            OutputStream outputStream = socket.getOutputStream();
//            // 封装Request对象和Response对象
//            Request request = new Request(inputStream);
//            Response reSponse = new Response(outputStream);
//
//            if (servletMap.get(request.getUrl()) == null) {
//                reSponse.outputHtml(request.getUrl());
//            } else {
//                // 动态资源servlet
//                HttpServlet httpServlet = servletMap.get(request.getUrl());
//                httpServlet.service(request, reSponse);
//            }
//            socket.close();
//        }
        // Minicat 4.0
        // 需求：请求动态资源（Servlet）,多线程（不使用线程池）
//        while (true) {
//            Socket socket = serverSocket.accept();
//            RequestProcessor requestProcessor = new RequestProcessor(socket, servletMap);
//            requestProcessor.start();
//        }
        // Minicat 4.0
        // 需求：请求动态资源（Servlet）,多线程（使用线程池）
//        while (true) {
//            System.out.println("===============>>>>>>> 使用线程池进行多线程改造");
//            Socket socket = serverSocket.accept();
//            RequestProcessor requestProcessor = new RequestProcessor(socket, servletMap);
//            threadPoolExecutor.execute(requestProcessor);
//        }
        // Minicat 5.0
        // 需求：tomcat部署多个项目，并根据不同项目加载对应的servlet
        while (true) {
            System.out.println("===============>>>>>>> 使用线程池进行多线程改造");
            Socket socket = serverSocket.accept();
            RequestProcessor requestProcessor = new RequestProcessor(socket, servletMap);
            threadPoolExecutor.execute(requestProcessor);
        }

    }

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
