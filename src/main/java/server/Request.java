package server;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author liJin
 * @date 2021/1/10 16:59
 * @desc 封装Request对象，需要依赖于inputStream
 */
public class Request {

    private String method;
    private String url;
    // 从输入流中获取其他属性
    private InputStream inputStream;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public Request() {
    }

    public Request(InputStream inputStream) throws IOException {
        this.inputStream = inputStream;
        // 从输入流中获取请求信息
        int count = 0;
        while (count == 0) {
            count = inputStream.available();
        }
        byte[] bytes = new byte[count];
        inputStream.read(bytes);
        String inputStr = new String(bytes);
        // 获取第一行请求头信息
        String fisrtLineStr = inputStr.split("\\n")[0];   // GET / HTTP/1.1
        String[] settings = fisrtLineStr.split(" ");
        this.method = settings[0];
        this.url = settings[1];

        System.out.println(" method    "+this.method);
        System.out.println(" url    "+this.url);
    }
}
