package server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author liJin
 * @date 2021/1/10 16:59
 * @desc 封装Response对象，需要依赖于OutputStream
 */
public class Response {

    private OutputStream outputStream;

    public Response() {

    }

    public Response(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public void outPut(String content) throws IOException {
        outputStream.write(content.getBytes());
    }

    /**
     * 根据url来获取静态资源的绝对路径，进一步根据绝对路径去读该静态资源文件，最终通过输出流输出
     * @param path
     */
    public void outputHtml(String path) throws IOException {
        // 获取静态资源文件的绝对路径
        String absoluteResourcePath = StaticResourceUtil.getAbsolutePath(path);
        // 输入静态资源文件
        File file = new File(absoluteResourcePath);
        if (file.exists() && file.isFile()) {
            // 输出静态资源
            StaticResourceUtil.outputStaticResource(new FileInputStream(file), outputStream);
        } else {
            // 输出404
            outPut(HttpProtocolUtil.getHttpHeader404());
        }
    }
}
