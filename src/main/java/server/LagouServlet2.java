package server;

import java.io.IOException;

/**
 * @author liJin
 * @date 2021/1/10 20:34
 * @desc
 */
public class LagouServlet2 extends HttpServlet {

    @Override
    public void doGet(Request request, Response response) {
        String content = "<h1>MyServlet get 222</h1>";
        try {
            response.outPut(HttpProtocolUtil.getHttpHeader200(content.getBytes().length) + content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(Request request, Response response) {
        String content = "<h1>MyServlet post 222</h1>";
        try {
            response.outPut(HttpProtocolUtil.getHttpHeader200(content.getBytes().length) + content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws Exception {

    }

    @Override
    public void destory() throws Exception {

    }
}
