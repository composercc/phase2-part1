package server;

/**
 * @author liJin
 * @date 2021/1/10 20:29
 * @desc
 */
public interface Servlet {

    void init() throws Exception;

    void destory() throws Exception;

    void service(Request request,Response response) throws Exception;

}
